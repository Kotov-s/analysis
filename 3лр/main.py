import pandas as pd

data = pd.read_csv('src/lab3_1.csv')

data['credit'] = data['credit'].apply(lambda x: 'С кредитом' if x == 1 else 'Без кредита')

transactions = []
for i in range(len(data)):
    row = data.iloc[i]
    transaction = [col for col in data.columns if row[col] == 1 or row[col] == 1]
    transaction.insert(0, row['credit'])
    transactions.append(transaction)

# print(transactions[20:50])

with open('src/basket.csv', 'w', encoding='utf8') as f:
    for transaction in transactions:
        f.write(','.join(transaction) + '\n')
