```python
import pandas as pd

data = pd.read_csv('src/lab3_1.csv')

data['credit'] = data['credit'].apply(lambda x: 'С кредитом' if x == 1 else 'Без кредита')

transactions = []
for i in range(len(data)):
    row = data.iloc[i]
    transaction = [col for col in data.columns if row[col] == 1 or row[col] == 1]
    transaction.insert(0, row['credit'])
    transactions.append(transaction)

with open('src/basket.csv', 'w', encoding='utf8') as f:
    for transaction in transactions:
        f.write(','.join(transaction) + '\n')

```

↑ Преобразование исходных данных в файл транзакций с помощью ЯП Python можно выполнить с помощью следующего кода.

![Alt text](imgs/image.png)

Проверить правильность заполненного списка

![Alt text](imgs/image-1.png)

Рис 1. Сводная информация о наборе транзакций

![Alt text](imgs/image-2.png)

Рис 2. Частотная диаграмма транзакций

![Alt text](imgs/image-3.png)

Рис 3. Сводная информация о ассоциативных правилах

![Alt text](imgs/image-4.png)

Рис 4. Сеть ассоциативных правил

![Alt text](imgs/image-7.png)

Рис 5. Граф ассоциативных правил

