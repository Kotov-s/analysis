| | Модель 1 | Модель 2 | Модель 3|
|-|----------|----------|---------|
| 1. Прогнозная модель | тренд | (тренд+сезоннаясоставляющая) | (тренд +сезонная составляющая+авторегрессионная составляющая |
| 2. Минимальный остаток (Min error) | -86.27938 | -83.19323 | -21.17143 |
| 3. Максимальный остаток (Max error) | 57.10858 | 52.84001 | 18.31644 |
| 4. Средняя ошибка (Mean error) | 1.073396e-15  | 6.476048e-15 | 0.3903172 |
| 5. СКО ошибки (Std. dev.)  | 34.70841 | 33.98783 | 7.331325 |
| 6. Средняя абсолютная ошибка (Mean absolute error) | 28.57336 | 28.06915 | 5.726612 |
| 7. Средняя ошибка в процентах (Mean percentage error) | -28.54865 | -27.74816 | 0.2272754 |
| 8. Средняя абсолютная ошибка в процентах (Mean absolute percentage error)  | 44.82989 | 43.96617 | 4.722847 |
| 9. Средний квадрат ошибки (Root mean squared error)| 34.61791 | 33.8992 | 7.322618 |
| 10. Коэффициент детерминации | 0.5172958 | 0.5371306 | 0.9784021 |


## Для первой модели
```r
library(tseries)
library(forecast)
library(TSA)

data <- read.table("n2lr.csv", header = TRUE, sep = ",")

data[1:4]
write.csv(data[1:4,], file = "my_data.csv")

tsData <- data[,4]
tsData

tsData <- ts(tsData, frequency=12, start=c(2007,1))
tsData

plot.ts(tsData)

tsDataComponents <- decompose(tsData)
tsDataComponents
plot(tsDataComponents)

myfit<-nls(var1 ~ a0 + a1*t + a2*t^2 + a5*t^3 + a6*t^0.8+ a7*t^8, data=data, start=c(a0=1, a1=1, a2=1, a5=1, a6=1, a7=1))

plot(myfit)

r1<-residuals(myfit)
plot(r1)

residuals <- residuals(myfit)

# Построение графика исходного временного ряда
plot(tsData, main="Time Series with Fitted Model", type="l", lty=1, col="black")

# Создание нового временного ряда с предсказанными значениями
fitted_values <- ts(fitted(myfit), start=start(tsData), frequency=frequency(tsData))

# Добавление предсказанных значений на график
lines(fitted_values, col="red")

mypr(residuals, tsData)

sp <- spec.pgram(residuals, detrend=FALSE, log="no", fast=FALSE, pad=FALSE, taper=0, plot=TRUE)
sp

tsAcf <- Acf(residuals, lag.max = 15, plot=TRUE, na.action = na.contiguous, demean = TRUE)

tsPacf <- pacf(residuals,lag.max = 15, plot = TRUE)

mypr <- function(x,y)
{
  ME <- mean(x)
  SD <- sd(x)
  Min <- min(x)
  Max <- max(x)
  MAE <- mean(abs(x))
  MPE <- mean(100*x/y)
  MAPE <- mean(abs(100*x/y))
  RMSE <- sqrt(mean(x*x))  
  sst <- sum((y - mean(y))^2)
  ssr <- sum(x^2)
  r_squared <- 1 - (ssr / sst)
  return(data.frame(SD=SD, Min=Min, Max=Max, ME=ME, MAE=MAE, MPE=MPE, MAPE=MAPE, RMSE=RMSE, r_squared=r_squared))
}

```

![Alt text](pics/m1/image.png)
![Alt text](pics/m1/image.png)
![Alt text](pics/m1/image-1.png)
![Alt text](pics/m1/image-2.png)
![Alt text](pics/m1/image-3.png)
![Alt text](pics/m1/image-4.png)
![Alt text](pics/m1/image-5.png)

```
> mypr(residuals, tsData)
        SD       Min      Max           ME      MAE       MPE     MAPE     RMSE r_squared
1 13.69819 -33.69428 37.47956 3.800504e-09 11.01367 -1.128307 14.56987 13.66247 0.9248139
```


## Для второй модели


```r
library(tseries)
library(forecast)
library(TSA)

# Загрузка данных
data <- read.table("n2lr.csv", header = TRUE, sep = ",")

# Создание временного ряда
tsData <- ts(data$var1, frequency=12, start=c(2007,1))

# Декомпозиция временного ряда на тренд, сезонность и остатки
tsDataComponents <- decompose(tsData)

# Создание переменных для тренда и сезонности
data$t <- 1:nrow(data)
data$seasonal <- tsDataComponents$seasonal


# Построение модели линейной регрессии с учетом тренда и сезонности
model <- nls(var1 ~ k + a * b^t + a3*seasonal, data=data, start=list(k=1, a=-1, b=0.9, a3=1))


m1<-nls(y ~ a0+a1*t+a2*t^2,data=data1,start=c(a0=1,a1=1,a2=0.3))
# Построение модели линейной регрессии с учетом тренда и сезонности


# Построение графика исходного временного ряда
plot(tsData, main="Time Series with Fitted Model", type="l", lty=1, col="black")

# Создание нового временного ряда с предсказанными значениями
fitted_values <- ts(fitted(model), start=start(tsData), frequency=frequency(tsData))

# Добавление предсказанных значений на график
lines(fitted_values, col="red")

residuals <- residuals(model)

mypr(residuals, tsData)

sp <- spec.pgram(residuals, detrend=FALSE, log="no", fast=FALSE, pad=FALSE, taper=0, plot=TRUE)
sp

summary(model)
plot(model)

accuracy(model)
# Вычисление остатков

# Вычисление минимальной и максимальной ошибки
min_error <- min(residuals)
max_error <- max(residuals)

# Вычисление среднеквадратичного отклонения ошибки
std_dev_error <- sd(residuals)

# Вычисление коэффициента детерминации (R-квадрат)
r_squared <- summary(model)$r.squared

# Построение автокорреляционной функции для остатков
acf(residuals, lag.max = 15)

# Построение частичной автокорреляционной функции для остатков
pacf(residuals, lag.max = 15)


mypr <- function(x,y)
{
  ME <- mean(x)
  SD <- sd(x)
  Min <- min(x)
  Max <- max(x)
  MAE <- mean(abs(x))
  MPE <- mean(100*x/y)
  MAPE <- mean(abs(100*x/y))
  RMSE <- sqrt(mean(x*x))
  
  sst <- sum((y - mean(y))^2)
  ssr <- sum(x^2)
  r_squared <- 1 - (ssr / sst)
  return(data.frame(SD=SD, Min=Min, Max=Max, ME=ME, MAE=MAE, MPE=MPE, MAPE=MAPE, RMSE=RMSE, r_squared=r_squared))
}

```

![Alt text](pics/m2/image-2.png)
![Alt text](pics/m2/image-1.png)
![Alt text](pics/m2/image.png)


```mypr(residuals, tsData)
        SD       Min   Max           ME      MAE       MPE     MAPE     RMSE r_squared
1 10.92819 -25.00053 32.33 7.184656e-09 8.299282 0.1902531 11.06237 10.89969 0.9521471

```
# 3 часть

```r
library(tseries)
library(forecast)
library(TSA)

# Загрузка данных
data <- read.table("n2lr.csv", header = TRUE, sep = ",")

# Создание временного ряда
tsData <- ts(data$var1, frequency=12, start=c(2007,1))

# Построение модели ARIMA с учетом тренда и сезонности
model_arima <- auto.arima(tsData, seasonal = TRUE)

# Построение прогноза на 12 периодов вперед
forecast_arima <- forecast(model_arima, h = 12)

# Построение графика исходного временного ряда
plot(tsData, main="Time Series with Fitted Model", type="l", lty=1, col="black")

# Создание нового временного ряда с предсказанными значениями
fitted_values_arima <- ts(fitted(model_arima), start=start(tsData), frequency=frequency(tsData))

# Добавление предсказанных значений на график
lines(fitted_values_arima, col="blue")
# Добавление прогноза на график
lines(forecast_arima$mean, col="red")
# Вычисление остатков
residuals_arima <- residuals(model_arima)

# Построение автокорреляционной функции для остатков
acf(residuals_arima)

# Построение частичной автокорреляционной функции для остатков
pacf(residuals_arima)

# Вычисление и построение периодограммы
sp <- spec.pgram(residuals_arima, detrend=FALSE, log="no", fast=FALSE, pad=FALSE, taper=0, plot=TRUE)
sp

mypr <- function(x,y)
{
  ME <- mean(x)
  SD <- sd(x)
  Min <- min(x)
  Max <- max(x)
  MAE <- mean(abs(x))
  MPE <- mean(100*x/y)
  MAPE <- mean(abs(100*x/y))
  RMSE <- sqrt(mean(x*x))
  
  sst <- sum((y - mean(y))^2)
  ssr <- sum(x^2)
  r_squared <- 1 - (ssr / sst)
  return(data.frame(SD=SD, Min=Min, Max=Max, ME=ME, MAE=MAE, MPE=MPE, MAPE=MAPE, RMSE=RMSE, r_squared=r_squared))
}

mypr(residuals_arima, tsData)


```

![Alt text](pics/m3/image.png)
![Alt text](pics/m3/image-1.png)
![Alt text](pics/m3/image-2.png)
![Alt text](pics/m3/image-3.png)


```
> mypr(residuals, tsData)
        SD       Min      Max        ME      MAE        MPE     MAPE     RMSE r_squared
1 6.515022 -21.21402 14.88552 -1.177134 5.125479 -0.4520511 3.554724 6.603793 0.9824343
```